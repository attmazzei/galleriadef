package it.uniroma3.siw.galleriaNuova.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Autore {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false , length = 20 )
	private String nome;
	
	@Column(nullable = false , length = 20 )
	private String cognome;
	
	@Column(nullable = false , length = 10 )
	private String nazionalita;
	
	@Column(nullable = false , length = 10 )
	@Temporal(value = TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataN;
	
	 @Column(nullable = false , length = 10 )
	@Temporal(value = TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataM;
	
	@OneToMany(mappedBy = "autore", cascade = {CascadeType.ALL})
	private List<Quadro> listaQuadri;
	
	public Autore(){
		
	}
	
	
	//GETTER AND SETTER DI AUTORE 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNazionalita() {
		return nazionalita;
	}

	public void setNazionalita(String nazionalita) {
		this.nazionalita = nazionalita;
	}

	public Date getDataN() {
		return dataN;
	}

	public void setDataN(Date dataN) {
		this.dataN = dataN;
	}

	public Date getDataM() {
		return dataM;
	}

	public void setDataM(Date dataM) {
		this.dataM = dataM;
	}

	public List<Quadro> getListaQuadri() {
		return listaQuadri;
	}

	public void setListaQuadri(List<Quadro> listaQuadri) {
		this.listaQuadri = listaQuadri;
	}


}
