package it.uniroma3.siw.galleriaNuova.service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.galleriaNuova.model.Autore;
import it.uniroma3.siw.galleriaNuova.repository.AutoreRepository;

@Service 
@Transactional 
public class AutoreService {

	
	
	@Autowired
	private AutoreRepository autoreRepository;

	public AutoreService() {
		
	}
	
	public Autore inserisciAutore(Autore autore) {
		autoreRepository.save(autore);
		
		return autore;
	}

	public List<Autore> getAutori() {
		List<Autore> autori = autoreRepository.findAll();
		return autori;
	}
	
	public Autore getAutore(long id) {
	Optional<Autore> autore = autoreRepository.findById(id);
	return autore.orElse(null);
	}

	public void delete(Autore a){
		autoreRepository.delete(a);
	}

	public List<Autore> searchByAutore(String autore){
		List<Autore> autori = autoreRepository.searchAutoreWithJPQLQuery(autore);
		return autori;
	}
	
	public List<Autore> searchByNazionalita(String searchTerm){
		List<Autore> autori = autoreRepository.searchNazionalitaWithJPQLQuery(searchTerm);
		return autori;
	}
	
	
	
}
