package it.uniroma3.siw.galleriaNuova.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.galleriaNuova.model.Utente;

public interface UtenteRepository extends CrudRepository<Utente , Long>{

	List<Utente> findAll();
	
	Utente findByUsername(String username);
	
	
	
}
