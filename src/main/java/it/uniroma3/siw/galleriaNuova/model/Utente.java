package it.uniroma3.siw.galleriaNuova.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;



@Entity
public class Utente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	

	@Column(nullable = false , length = 50 )
	private String nome;
	

	@Column(nullable = false , length = 30 )
	private String cognome;
	
	@Column(nullable = false, length = 100)
	private String username;

	

	@Column(nullable = false , length = 100)
	private String password;
	
	private boolean enabled;
	
	@Column(nullable = false , length = 100 )
	private String email;
	

	
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
	private Ruolo ruoloUtente;
	
	public Utente(String email, String password, String nome, String cognome, String permessi, boolean enabled, Ruolo ruoloUtente){
		this.email = email;
		this.password = password;
		this.nome = nome;
		this.cognome = cognome;
		this.ruoloUtente = ruoloUtente;
		this.enabled = enabled;

	
		
		
	}

	
	
	public Utente(String username, String password, boolean enabled){
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		
		
		
	}
	
	
	
		public Utente(){
			
		}

		
		//GETTER AND SETTER DI UTENTE
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}


		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getCognome() {
			return cognome;
		}

		public void setCognome(String cognome) {
			this.cognome = cognome;
		}

		public Ruolo getRuoloUtente() {
			return ruoloUtente;
		}

		public void setRuoloUtente(Ruolo ruoloUtente) {
			this.ruoloUtente = ruoloUtente;
		}
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

}
