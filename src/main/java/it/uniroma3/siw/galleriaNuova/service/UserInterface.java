package it.uniroma3.siw.galleriaNuova.service;



import it.uniroma3.siw.galleriaNuova.model.Ruolo;
import it.uniroma3.siw.galleriaNuova.model.Utente;
import it.uniroma3.siw.galleriaNuova.validation.UsernameExistsException;

public interface UserInterface {
    Utente registraNuovoUtente(Utente u, Ruolo r) throws  UsernameExistsException;
}
