package it.uniroma3.siw.galleriaNuova.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.siw.galleriaNuova.model.Autore;
import it.uniroma3.siw.galleriaNuova.model.Quadro;
import it.uniroma3.siw.galleriaNuova.service.AutoreService;
import it.uniroma3.siw.galleriaNuova.service.QuadroService;
import it.uniroma3.siw.galleriaNuova.upload.ImmagineUpload;

@Controller
@SessionAttributes({"quadro"})
public class UploadController {

	@Autowired
	private QuadroService quadroService;
	@Autowired
	private AutoreService autoreService;
	@Autowired
	private ImmagineUpload upload;




	//CONTROLLER PER L'INSERIMENTO DEI QUADRI E DEGLI AUTORI DA PARTE DELL'ADMIN

	@RequestMapping(value = {"/inserimentoQuadro"} , method = RequestMethod.GET)
	public String inserimentoQuadro(Quadro quadro, Model model){
		List<Autore> autori = autoreService.getAutori();
		model.addAttribute("autori", autori);
		return "/inserimentoQuadro";
	}


	//INSERIAMO LE INFORMAZIONI DEL QUADRO PRIMA

	@RequestMapping(value = {"/inserimentoQuadro"} ,  method = RequestMethod.POST)
	public String inserimentoQuadro(@Validated @ModelAttribute Quadro quadro, BindingResult bindingResult,
			@RequestParam(value = "autoriEsistenti", required = false) Long autoriEsistenti, 
			@RequestParam(value = "inserisciAutore", required =false ) String inserisciAutore, 
			@RequestParam(value = "immagineQuadro", required = false) MultipartFile immagineQuadro,
			SessionStatus status, Model model){

		if(bindingResult.hasErrors()){

			List<FieldError> errors = bindingResult.getFieldErrors();
			for (FieldError error : errors ) {
				System.out.println (error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField() + " - " + error.getCode());
			}
			List<Autore> autori = autoreService.getAutori();
			model.addAttribute("autori", autori);
			return "/inserimentoQuadro";

		} 

		//SE L'AUTORE è GIA ESISTENTE SI PUO SEMPICEMENTE INSERIRE SOLO IL QUADRO CHE VERRA ASSOCIATO ALL'AUTORE

		if(inserisciAutore != null){
			//CARICO L'IMMAGINE QUADRO
			if (!immagineQuadro.isEmpty()) {
				upload.creaDirectoryImmaginiSeNecessario();
				upload.creaFileImmagine(immagineQuadro.getOriginalFilename(), immagineQuadro);
				quadro.setImmagine(immagineQuadro.getOriginalFilename());
			}
			Autore autore = autoreService.getAutore(autoriEsistenti);
			quadro.setAutore(autore);
			quadroService.inserisciQuadro(quadro);
			model.addAttribute("inseritoCorrettamente", true);
			model.addAttribute("quadro" , quadro);
			model.addAttribute("autore" , autore);
			status.setComplete();
		}else{
			
			//ALTRIMENTI CREI UN NUOVO AUTORE 
			
			model.addAttribute("quadro" , quadro);
			Autore autore = new Autore();
			model.addAttribute("autore" ,autore);

		}
		return "/inserimentoAutore";
	}


	//INSERIAMO LE INFORMAZIONI DELL'UTENTE DOPO 

	@RequestMapping(value = {"/inserimentoAutore"} , method = RequestMethod.POST)
	public String inserimentoAutore(@Validated @ModelAttribute Autore autore, BindingResult bindingResult, Quadro quadro,  @RequestParam(value = "immagineQuadro", required = false) MultipartFile immagineQuadro, Model model, SessionStatus status){

		if(bindingResult.hasErrors()){
			List<FieldError> errors = bindingResult.getFieldErrors();
			for (FieldError error : errors ) {
				System.out.println (error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField() + " - " + error.getCode());
			}
			return "inserimentoAutore";
		}
		System.out.println(immagineQuadro.getOriginalFilename());
		if (!immagineQuadro.isEmpty()) {
			upload.creaDirectoryImmaginiSeNecessario();
			upload.creaFileImmagine(immagineQuadro.getOriginalFilename(), immagineQuadro);
			quadro.setImmagine(immagineQuadro.getOriginalFilename());
		}


		quadro.setAutore(autore);
		quadroService.inserisciQuadro(quadro);

		status.setComplete();

		model.addAttribute("quadro", quadro);
		model.addAttribute("inseritoCorrettamente", true);
		return "/inserimentoAutore";
	}

}
