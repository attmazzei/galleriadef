package it.uniroma3.siw.galleriaNuova.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import it.uniroma3.siw.galleriaNuova.model.Autore;
import it.uniroma3.siw.galleriaNuova.model.Quadro;
import it.uniroma3.siw.galleriaNuova.model.Ruolo;
import it.uniroma3.siw.galleriaNuova.model.Utente;
import it.uniroma3.siw.galleriaNuova.service.AutoreService;
import it.uniroma3.siw.galleriaNuova.service.QuadroService;
import it.uniroma3.siw.galleriaNuova.service.UtenteService;
import it.uniroma3.siw.galleriaNuova.upload.ImmagineUpload;

@Controller
public class AdminController {

	@Autowired 
	private UtenteService utenteService;

	@Autowired
	private AutoreService autoreService;

	@Autowired
	private QuadroService quadroService;

	@Autowired
	private ImmagineUpload upload;




	@RequestMapping(value = {"/admin"} , method = RequestMethod.GET)
	public String pannelloDiControllo( Model model){
		return "admin/pannelloDiControllo";
	}


	//VISUALIZZA LA LISTA DEGLI UTENTI REGISTRATI

	@RequestMapping(value={"/listaUtenti"} , method = RequestMethod.GET)
	public String admin (Model model){
		List<Utente> utenti= utenteService.findAll();
		model.addAttribute("utenti",utenti);

		return "admin/listaUtenti";
	}

	//RESTITUISCE LE INFORMAZIONI RIGUARDANTI L'UTENTE

	@RequestMapping(value = {"/infoUtente"} , method = RequestMethod.GET)
	public String infoUtente(@ModelAttribute("id") Long id ,  Model model){
		Utente utente= utenteService.getUtente(id);
		model.addAttribute("utente", utente);

		return "admin/infoUtente";
	}


	//MODIFICA UTENTE SERVE PER LA MODIFICA DEL RUOLO DELL'UTENTE O SEMPLICEMENTE I DATI INSERITI ALLA REGISTRAZIONE 

	@RequestMapping(value={"/modificaUtente"} ,method = RequestMethod.GET )
	public String modifica(Model model,@ModelAttribute("id") Long id, BindingResult results){
		Utente utente= utenteService.getUtente(id);
		model.addAttribute(utente);

		return "admin/modificaUtente";
	}


	@RequestMapping(value = {"/modificaUtente"} ,method = RequestMethod.POST  )
	public String modifica(@Validated @ModelAttribute Utente utente,BindingResult results, HttpServletRequest request, @RequestParam(value="ruoloEsistenti",required= false) String ruolo,@RequestParam(value="RuoloUtente", required=false)String check, Model model){
		Long id=utente.getId();
		Ruolo r= utenteService.getUtente(id).getRuoloUtente();
		String username=utenteService.getUtente(id).getUsername();
		if(results.hasErrors()){

			List<FieldError> errors = results.getFieldErrors();
			for (FieldError error : errors ) {
				System.out.println (error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField() + " - " + error.getCode());
			}
		}

		//CONTROLLA CHE L'USERNAME NON SIA GIà ESISTENTE
		//SE NON LO è ALLORA FA LA MODIFICA SE LO è RITORNA SULLA PAGINA DI MODIFICA 

		if(!username.equals(utente.getUsername())){
			if(utenteService.getUtenteByUsername(utente.getUsername()) != null){
				model.addAttribute(utente);
				model.addAttribute("usernameEsistente", true);
				return "/admin/modificaUtente";
			}else{ 
				r.setUsername(utente.getUsername());

			}
		}
		System.out.println(utente.getPassword());
		if(utente.getPassword().equals("")|| utente.getPassword()==null){
			String vecchiaPassword= utenteService.getUtente(id).getPassword();
			utente.setPassword(vecchiaPassword);
		}else{
			String password = new BCryptPasswordEncoder().encode(utente.getPassword());
			utente.setPassword(password);
		}

		//in caso ci fosse da modificare il ruolo dell'utente 
		//se non si vuole  
		if(check == null){
			utente.setRuoloUtente(r);
		}else{ //se si vuole
			r.setRole(ruolo);
			utente.setRuoloUtente(r);
		}
		utente.setEnabled(true);
		utenteService.modificaUtente(utente);
		return "redirect:/infoUtente?id=" + String.valueOf(id);
	}

	//RIMUOVE UN UTENTE DALLA LISTA UTENTI 

	@RequestMapping(value= {"/rimuoviUtente"} , method = RequestMethod.GET )
	public String rimuovi(Model model,@ModelAttribute("id") Long id, BindingResult results){
		if(results.hasErrors()){
			List<Utente> utenti =utenteService.findAll();
			model.addAttribute("utenti", utenti);

			return"/admin/listaUtenti";
		}else{
			model.addAttribute("utente", utenteService.getUtente(id));
			utenteService.delete(utenteService.getUtente(id));
			model.addAttribute("cancellato", true);
			List<Utente> utenti = utenteService.findAll();
			model.addAttribute("utenti", utenti);

			return"/admin/listaUtenti";
		}
	}

	//SERVE PER LA MOFICA DEI DATI DELL'AUTORE 


	@RequestMapping(value={"/modificaAutore"} ,  method = RequestMethod.GET)
	public String modificaAutore(@ModelAttribute("id") Long id, Model model){

		Autore autore = autoreService.getAutore(id);

		model.addAttribute("autore", autore);

		return "admin/modificaAutore";
	}

	@RequestMapping(value={"/modificaAutore"} , method = RequestMethod.POST )
	public String modificaAutore(@Validated @ModelAttribute Autore autore, BindingResult results, RedirectAttributes ra, Model model){
		if(results.hasErrors()){
		}



		autoreService.inserisciAutore(autore);
		ra.addFlashAttribute("modificatoCorrettamente", true);
		return "redirect:/infoAutore?id="+String.valueOf(autore.getId());

	}

	//ELIMINA AUTORI ESISTENTI NELLA LISTA AUTORI 

	@RequestMapping(value={"/rimuoviAutore"} , method = RequestMethod.GET)
	public String rimoviAutore(@ModelAttribute("id") Long id, RedirectAttributes ra, Model model){
		Autore autore = autoreService.getAutore(id);
		autoreService.delete(autore);
		ra.addFlashAttribute("autoreCancellatoCorrettamente",true);
		return "redirect:/listaQuadri";
	}



	//MODIFICA I DATI DEL QUADRO E LA RELATIVA IMMAGINE 


	@RequestMapping(value={"/modificaQuadro"} , method = RequestMethod.GET)
	public String infoQuadro(@ModelAttribute("id") Long id, Model model){
		Quadro quadro = quadroService.getQuadro(id);
		model.addAttribute("quadro", quadro);

		return "admin/modificaQuadro";
	}

	@RequestMapping(value={"/modificaQuadro"} , method = RequestMethod.POST)
	public String infoQuadro(@Validated @ModelAttribute Quadro quadro,BindingResult results, RedirectAttributes ra, @RequestParam(value = "immagineQuadro", required = false) MultipartFile immagineQuadro, Model model){
		if(results.hasErrors()){
		}



		if (!immagineQuadro.isEmpty()) {
			upload.creaDirectoryImmaginiSeNecessario();
			upload.rimuovi(quadro.getImmagine());
			upload.creaFileImmagine(immagineQuadro.getOriginalFilename(), immagineQuadro);
			quadro.setImmagine(immagineQuadro.getOriginalFilename());
		}
		Autore autore = autoreService.getAutore(quadro.getAutore().getId());
		quadro.setAutore(autore);
		quadroService.inserisciQuadro(quadro);
		ra.addFlashAttribute("modificatoCorrettamente",true);


		return "redirect:/infoQuadro?id="+String.valueOf(quadro.getId());

	}

	//RIMUOVI QUADRO

	@RequestMapping(value="/rimuoviQuadro" , method = RequestMethod.GET)
	public String rimoviQuadro(@ModelAttribute("id") Long id, RedirectAttributes ra,Model model){
		Quadro quadro = quadroService.getQuadro(id);
		upload.rimuovi(quadro.getImmagine());
		quadroService.delete(quadro);
		ra.addFlashAttribute("quadroCancellatoCorrettamente", true);
		return "redirect:/listaQuadri";
	}

}
