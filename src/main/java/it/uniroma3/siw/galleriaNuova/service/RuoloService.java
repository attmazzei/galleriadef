package it.uniroma3.siw.galleriaNuova.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.galleriaNuova.model.Ruolo;
import it.uniroma3.siw.galleriaNuova.repository.RuoloRepository;

@Service
@Transactional
public class RuoloService {


	@Autowired
	private RuoloRepository repository;
	
	public RuoloService(){
		
	}
	
	public void inserisciRuoloUtente(Ruolo r){
		repository.save(r);
	}
}
