	package it.uniroma3.siw.galleriaNuova.service;

import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.galleriaNuova.model.Autore;
import it.uniroma3.siw.galleriaNuova.model.Quadro;
import it.uniroma3.siw.galleriaNuova.repository.QuadroRepository;

@Service
@Transactional
public class QuadroService {
	
	@Autowired
	private QuadroRepository quadroRepository;

	public QuadroService() {
		
	}
	
	public void inserisciQuadro(Quadro quadro) {
		quadroRepository.save(quadro);
		
	}

	public List<Quadro> getQuadri() {
		List<Quadro> autori = quadroRepository.findAll();
		return autori;
	}
	
	public Quadro getQuadro(long id) {
		Optional<Quadro> quadro = quadroRepository.findById(id);
		return quadro.orElse(null);
	 }

	public void delete(Quadro q){
		quadroRepository.delete(q);
	}


	
	public List<Quadro> getQuadriByAutore(Autore autore){
		List<Quadro> quadri = quadroRepository.findByAutore(autore);
		return quadri;
	}
	
	public List<Quadro> getQuadroByAnno(Integer anno){
		List<Quadro> quadri = quadroRepository.findByAnno(anno);
		return quadri;
	}
	
	public List<Quadro> searchByTitolo(String titolo){
		List<Quadro> quadriSearch = quadroRepository.searchTitoloWithJPQLQuery(titolo);
		return quadriSearch;
	 }
}
