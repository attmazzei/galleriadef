package it.uniroma3.siw.galleriaNuova.configuration;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class AuthConfiguration extends WebSecurityConfigurerAdapter {
	   
	@Autowired
	 private DataSource dataSource;
	   
	   @Override
	   protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		
	
		
		 
			 	auth.jdbcAuthentication().dataSource(dataSource)
			.passwordEncoder(new BCryptPasswordEncoder())
			.usersByUsernameQuery("SELECT username,password,1  FROM utente where username=?")
			.authoritiesByUsernameQuery("SELECT username,ruolo FROM ruolo where username=?");
		}
	   
	   
	
	  @Override
	 protected void configure(HttpSecurity http) throws Exception {

    	
        http
        .authorizeRequests()
        	.antMatchers("/","/index", "/registration").permitAll()
        .antMatchers("/inserimento/**").access("hasAuthority('ADMIN')")
        	.antMatchers("/admin/**").access("hasAuthority('ADMIN')")
            .anyRequest().authenticated()
            .and()
        .formLogin()
            .loginPage("/login").usernameParameter("username").passwordParameter("password")
            .permitAll()
            .failureUrl("/?error")
            .and()
        .rememberMe()
        	.key("$2a$04$dbc42eVSPP.GzzwGVzjkP.AGUeZuiO/2tdGaFReM17WzmJ9o7DsMW")
        	.rememberMeParameter("ricordami")
        	.and()
        .logout()
        	.logoutSuccessUrl("/?logout")
            .permitAll()
        	.and()
        .sessionManagement()
        	.maximumSessions(1)
        	.expiredUrl("/login?expired");
    }
	  
	  
	  
	  
	  
	  
	  
	  
}
