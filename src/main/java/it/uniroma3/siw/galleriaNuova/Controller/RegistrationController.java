package it.uniroma3.siw.galleriaNuova.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.galleriaNuova.model.Ruolo;
import it.uniroma3.siw.galleriaNuova.model.Utente;
import it.uniroma3.siw.galleriaNuova.service.UtenteService;
import it.uniroma3.siw.galleriaNuova.validation.UsernameExistsException;


@Controller

public class RegistrationController {
	
	@Autowired
	private UtenteService utenteService;
	
	
	//REGISTRAZIONE UTENTE
	
	@RequestMapping(value = { "/registration"} , method = RequestMethod.GET)
	public String registration(@ModelAttribute Utente utente, Model model){
		return "registration";
	}
	
	
	
	
	//CI SIAMO CREATI UN FORM DI REGISTRAZIONE CON LA CONVALIDA DEI DATI 
	
	@RequestMapping(value = {"/registration" }, method = RequestMethod.POST)
	public String registraUtente(@Validated @ModelAttribute Utente utente, BindingResult bindingResult, HttpServletRequest request, Model model){

        
		utente.setEnabled(true);
		Ruolo r = new Ruolo("USER", utente.getUsername());
	    Utente u = new Utente();
	    
	
	    if (!bindingResult.hasErrors()) {
	        u = creaAccountUtente(utente, bindingResult, r);
	    }
	    
	    if (u == null) {
	    	bindingResult.rejectValue("username", "Username.Existing");
	    }
		
		if(bindingResult.hasErrors()){
		    List<FieldError> errors = bindingResult.getFieldErrors();
		    for (FieldError error : errors ) {
		        System.out.println (error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField() + " - " + error.getCode());
		    }
		    model.addAttribute("utente", utente);
			return "/registration";
		}else {
			model.addAttribute("utente", u);
			model.addAttribute("utenteInserito", true);
			return "/index";
	    }

	}

	
	//METODO DI SUPPORTO PER LA CREAZIONE UTENTE
	//NE CREA UNO NUOVO SE NON CI SONO 
	private Utente creaAccountUtente(Utente utente, BindingResult result, Ruolo r) {
	    Utente u = null;
	    try {
	        u = utenteService.registraNuovoUtente(utente, r);
	    }//ALTRIMENTRI
	    catch (UsernameExistsException e) {
	        
	    	return null;
	    }
	    return u;
	}
}
