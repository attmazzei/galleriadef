package it.uniroma3.siw.galleriaNuova.service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.galleriaNuova.model.Ruolo;
import it.uniroma3.siw.galleriaNuova.model.Utente;
import it.uniroma3.siw.galleriaNuova.repository.UtenteRepository;
import it.uniroma3.siw.galleriaNuova.validation.UsernameExistsException;

@Service
@Transactional
public class UtenteService  implements UserInterface{
	
	@Autowired    
	private UtenteRepository repository;
	
	
	
	public UtenteService() {
		
	}
	 
	public Utente getUtente(Long id) {
		Optional<Utente> utente = repository.findById(id);
		return utente.orElse(null);
	}
	 
	public void delete(Utente u){
		repository.delete(u);
	}
	
	public Utente getUtenteByUsername(String username){
		Utente utente = repository.findByUsername(username);
		return utente;
	}
	 
	public List<Utente> findAll(){
		List<Utente> utenti= repository.findAll();
		return utenti;
	}
	
	public void modificaUtente(Utente utente){
		repository.save(utente);
	}

	

		@Override
		public Utente registraNuovoUtente(Utente utente, Ruolo ru) throws  UsernameExistsException{
	         
	        if (usernameEsiste(utente.getUsername())) {   
	            throw new UsernameExistsException("Username gia' esistente");
	        }
	        utente.setRuoloUtente(ru);
	        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String password = passwordEncoder.encode(utente.getPassword());
			utente.setPassword(password);
	        return repository.save(utente);       
	    }

	    private boolean usernameEsiste(String username) {
	        Utente utente = repository.findByUsername(username);
	        if (utente != null) {
	            return true;
	        }
	       return false;
	    }
	

}
