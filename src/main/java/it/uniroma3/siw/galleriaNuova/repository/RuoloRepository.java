package it.uniroma3.siw.galleriaNuova.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.galleriaNuova.model.Ruolo;


public interface RuoloRepository extends CrudRepository<Ruolo , Long>{

	
	
	
}
