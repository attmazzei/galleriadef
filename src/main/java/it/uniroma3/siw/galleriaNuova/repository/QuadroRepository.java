package it.uniroma3.siw.galleriaNuova.repository;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.uniroma3.siw.galleriaNuova.model.Autore;
import it.uniroma3.siw.galleriaNuova.model.Quadro;


public interface QuadroRepository extends CrudRepository<Quadro , Long>{
	 

	List<Quadro> findAll();
	List<Quadro> findByAutore(Autore autore);
	List<Quadro> findByAnno(Integer anno);
	
	@Query("SELECT q FROM Quadro q WHERE LOWER(q.titolo) LIKE LOWER(CONCAT('%',:searchTerm,'%'))")
    public List<Quadro> searchTitoloWithJPQLQuery(@Param("searchTerm") String searchTerm);
    
	
}
