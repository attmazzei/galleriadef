package it.uniroma3.siw.galleriaNuova.Controller;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import it.uniroma3.siw.galleriaNuova.model.Autore;
import it.uniroma3.siw.galleriaNuova.model.Quadro;
import it.uniroma3.siw.galleriaNuova.service.AutoreService;
import it.uniroma3.siw.galleriaNuova.service.QuadroService;
import it.uniroma3.siw.galleriaNuova.upload.ImmagineUpload;

@Controller
public class MainController {


	@Autowired
	private QuadroService quadroService;
	@Autowired
	private AutoreService autoreService;

	@Autowired
	private ImmagineUpload upload;




	//ANTEPRIMA DI ALCUNI QUADRI SOLO LOG IN 

	@RequestMapping(value = { "/anteprima"} , method = RequestMethod.GET)
	public String anteprima(){
		return "anteprima";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String indexSlash(){
		return "index";
	}

	@RequestMapping(value = "/index" , method = RequestMethod.GET)
	public String index(SessionStatus status){
		status.setComplete();
		return "index";
	}


	//LISTA QUADRI PRESENTI NELLA GALLERIA 


	@RequestMapping(value = {"/listaQuadri"} , method = RequestMethod.GET)
	public String lista(Model model){
		List<Quadro> quadri = quadroService.getQuadri();	
		model.addAttribute("quadri",quadri);
		List<Autore> autori = autoreService.getAutori();
		model.addAttribute("autori",autori);
		return "listaQuadri";
	}




	//INFORMAZIONI QUADRO RESTITUISCE TUTTI I DATI DEL QUADRO 

	@RequestMapping(value = {"/infoQuadro"} , method = RequestMethod.GET)
	public String dettagliQuadro(@ModelAttribute("id") Long id, BindingResult results, Model model){

		if(results.hasErrors()){
			return "listaQuadri";
		}

		Quadro quadro = quadroService.getQuadro(id);
		model.addAttribute("quadro" , quadro);
		return "infoQuadro";
	}


	//INFORMAZIONI AUTORE 

	@RequestMapping(value = {"/infoAutore"} , method = RequestMethod.GET)
	public String infoAutore(@ModelAttribute("id") Long id, BindingResult results, Model model){

		if(results.hasErrors()){
			return "listaQuadri";
		}

		Autore autore = autoreService.getAutore(id);
		model.addAttribute(autore);
		List<Quadro> quadri = quadroService.getQuadriByAutore(autore);
		model.addAttribute("quadri",quadri);
		return "/infoAutore";
	}


	//BARRA DI RICERCA PER CATEGORIE 


	@RequestMapping(value = {"/ricerca"} , method = RequestMethod.POST)
	public String ricerca(@ModelAttribute("selectRicerca") String scelta, @ModelAttribute("searchBox") String ricerca, RedirectAttributes ra, Model model){
		List<Quadro> quadri = new ArrayList<Quadro>();
		List<Autore> autori = new ArrayList<Autore>();

		//POPOLIAMO LE LISTE TRAMITE LA SELEZIONE DEI CAMPI DI RICERCA A TENDINA 

		switch(scelta){
		case "1": quadri = quadroService.searchByTitolo(ricerca);
		break;
		case "2": quadri = quadroService.getQuadroByAnno(Integer.valueOf(ricerca));
		break;
		case "3": autori = autoreService.searchByAutore(ricerca);
		break;
		case "4": autori = autoreService.searchByNazionalita(ricerca);
		break;
		}

		//SE LE LISTE SONO VUOTE ALLORA RITORNA LA LISTA DI TUTTI I QUADRI
		if(autori.isEmpty() && quadri.isEmpty()){
			ra.addFlashAttribute("nessunRisultato", true);
			return "redirect:/listaQuadri";

		}else if(!autori.isEmpty()){
			for(Autore autore: autori){
				for(Quadro quadro: quadroService.getQuadriByAutore(autore)){
					quadri.add(quadro);
				}
			}
			//ALTRIMENTI HO CERCATO IL QUADRO 
		}else{
			for(Quadro quadro: quadri){
				autori.add(quadro.getAutore());
			}
		}
		model.addAttribute("autori",autori);
		model.addAttribute("quadri",quadri);
		return "listaQuadri";
	}




	//RITORNA L'IMMAGINE INSERITA DURANTE L'INSERIMENTO DEL QUADRO 


	@RequestMapping(value = {"/quadro/{nomeImmagine}"} , method = RequestMethod.GET)
	@ResponseBody
	public byte[] getImage(@PathVariable(value = "nomeImmagine") String nomeImmagine) throws IOException {
		File serverFile = upload.getPathFile(nomeImmagine);

		return Files.readAllBytes(serverFile.toPath());
	}


	// LOGIN FORM
	@RequestMapping("/login")
	public String login() {
		return "login";
	}

}
