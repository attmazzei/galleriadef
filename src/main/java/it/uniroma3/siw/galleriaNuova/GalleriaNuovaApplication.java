package it.uniroma3.siw.galleriaNuova;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class GalleriaNuovaApplication  extends SpringBootServletInitializer {

	
   @Override 
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(GalleriaNuovaApplication.class);
    }

	
	public static void main(String[] args) {
		SpringApplication.run(GalleriaNuovaApplication.class, args);
	}

}
